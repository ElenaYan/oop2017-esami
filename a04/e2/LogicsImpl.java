package a04.e2;

import java.util.*;

public class LogicsImpl implements Logics {
	private List<Integer> list = new LinkedList<>();
	private Map<Integer,Integer> counter = new HashMap<>();
	private Random rand = new Random(); 

	@Override
	public String randNumber(int index) {
		int num = rand.nextInt(6);
		if(list.size()<5) {
			list.add(index, num);
		}
		else {
			list.set(index,num);
		}
		return Integer.toString(num);
	}

	@Override
	public void printType() {
		for(Integer i : list) {
			int count = (int) this.list.stream().filter(e->e==i).count();
			this.counter.put(i, count);
		}
		Integer count = this.counter.entrySet().stream().mapToInt(e->e.getValue()).max().getAsInt();
		if(count==5) {
			System.out.println(Type.YAHTZEE);
		}
		else if(count==4) {
			System.out.println(Type.FOUR);
		}
		else if(count==3) {
			System.out.println(Type.THREE);
		}		
		else {
			System.out.println(Type.NOTHING);
		}
		
		this.counter.clear();
	}

}
