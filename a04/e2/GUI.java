package a04.e2;

import java.awt.*;
import javax.swing.*;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;


public class GUI extends JFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8976182355465144523L;
	private static final int SIZE = 5;
	private List<JButton> list = new LinkedList<>(); 
	private Logics logics = new LogicsImpl();

	public GUI(){
	
		this.setSize(500, 100);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.getContentPane().setLayout(new FlowLayout());
		
		ActionListener ac = e -> {
			final JButton jb = (JButton)e.getSource();
			jb.setEnabled(false);		
		};
		
		ActionListener dr = e -> {
			for(JButton jb : list) {
				if(!jb.isEnabled()) {
					jb.setText(this.logics.randNumber(list.indexOf(jb)));
					jb.setEnabled(true);
				}
			}
			logics.printType();
		};
		
		for(int i=0; i<SIZE; i++) {
			JButton jb1 = new JButton(logics.randNumber(i));
			jb1.addActionListener(ac);
			this.getContentPane().add(jb1);
			this.list.add(jb1);
		}
		
		JButton draw = new JButton("Draw");
		draw.addActionListener(dr);
		this.getContentPane().add(draw);

		this.setVisible(true);
	}
}

