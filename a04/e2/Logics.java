package a04.e2;

public interface Logics {
	
	String randNumber(int index); 
	
	void printType();
	
	
	public static enum Type {
		STRAIGHT,
		YAHTZEE,
		FOUR,
		FULL,
		THREE,
		NOTHING;
	}
}
