package a04.e1;

import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class TurnamentImpl implements Turnament {
	List<Player> players = new LinkedList<>();
	Set<Match> matchs = new HashSet<>();
	Boolean isStart = false;
	List<Player> winners = new LinkedList<>();

	@Override
	public Player makePlayer(int id, String name) {
		return new PlayerImpl(id, name);
	}

	@Override
	public Match makeMatch(Player p1, Player p2) {
		Match match = new MatchImpl(p1, p2);
		this.matchs.add(match);
		return match;
	}

	@Override
	public void registerPlayer(Player player) {
		this.players.add(player);
	}

	@Override
	public void startTurnament() {
		if(this.matchs.size()==0) {
			for(int i=0; i<this.players.size(); i++) {
				this.matchs.add(this.makeMatch(this.players.get(i), this.players.get(++i)));
			}
		}
		this.isStart=true;
	}

	@Override
	public List<Player> getPlayers() {
		return this.players;
	}

	@Override
	public List<Match> getPendingGames() {
		List<Match> list = new LinkedList<>();
		for(Match match : this.matchs) {
			list.add(match);
		}
		return list;
	}

	@Override
	public void playMatch(Match match, Player winner) {
		this.matchs.remove(match);
		this.winners.add(winner);
		if(this.matchs.size()==0) {
			Player p1 = null;	
			for(Player p : this.players) {				
				if(this.winners.contains(p)) {
					if(p1==null) {
						p1=p;
					} else {
						this.matchs.add(this.makeMatch(p1, p));
						p1=null;
					}
				}
			}
			this.winners = new LinkedList<>();
		}
	}

	@Override
	public boolean isTurnamentOver() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Player winner() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<Player> opponents(Player player) {
		// TODO Auto-generated method stub
		return null;
	}

}
