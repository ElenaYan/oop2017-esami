package a01a.e1;

import java.util.Iterator;
import java.util.List;

public class InfiniteSequenceOpsImpl implements InfiniteSequenceOps {

	@Override
	public <X> InfiniteSequence<X> ofValue(X x) {
		return () -> x;
	}

	@Override
	public <X> InfiniteSequence<X> ofValues(List<X> l) {
		return  new InfiniteSequence<X> () {
			int counter = 0;

			@Override
			public X nextElement() { 
				if(this.counter<l.size()) {
					return l.get(counter++);
				}
				else {
					this.counter = 0;
					return l.get(counter++);
				}
			}
			
		};
	}

	@Override
	public InfiniteSequence<Double> averageOnInterval(InfiniteSequence<Double> iseq, int intervalSize) {
		return new InfiniteSequence<Double>() {

			@Override
			public Double nextElement() {
				return iseq.nextListOfElements(intervalSize).stream().mapToDouble(e -> e).average().getAsDouble();
			}
			
		};
	}

	@Override
	public <X> InfiniteSequence<X> oneEachInterval(InfiniteSequence<X> iseq, int intervalSize) {
		return new InfiniteSequence<X>() {

			@Override
			public X nextElement() {
				for(int i=1 ; i<intervalSize; i++) {
					iseq.nextElement();
				}
				return iseq.nextElement();
			}
			
		};
	} 

	@Override
	public <X> InfiniteSequence<Boolean> equalsTwoByTwo(InfiniteSequence<X> iseq) {
		return new InfiniteSequence<Boolean>() {

			@Override
			public Boolean nextElement() {
				X a = iseq.nextElement();
				X b = iseq.nextElement();
				return (a==b);
			}
			
		};
	}

	@Override
	public <X, Y extends X> InfiniteSequence<Boolean> equalsOnEachElement(InfiniteSequence<X> isx,
			InfiniteSequence<Y> isy) {
		return new InfiniteSequence<Boolean>() {

			@Override
			public Boolean nextElement() {
				X a = isx.nextElement();
				X b = isy.nextElement();
				return a==b;
			}
			
		};
	}

	@Override
	public <X> Iterator<X> toIterator(InfiniteSequence<X> iseq) {
		return new Iterator<X>() {

			@Override
			public boolean hasNext() {
				return true;
			}

			@Override
			public X next() {

				return iseq.nextElement();
				
			}
			
		};
	}

}
