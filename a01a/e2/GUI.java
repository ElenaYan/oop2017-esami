package a01a.e2;


import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.*;


public class GUI extends JFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4278655556760156615L;
	final private Logics logics = new LogicsImpl();
	//map to associate a JButton with a JButton position
	final Map <JButton, Integer> buttons = new HashMap<>();
	
	public GUI(int size){

		this.setSize(500, 100);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.getContentPane().setLayout(new FlowLayout());
		
		ActionListener ac = e -> {
			final JButton jb = (JButton)e.getSource();
			final Integer actualButton = buttons.get(jb);
			
			jb.setText(logics.setNumber(actualButton));
			jb.setEnabled(logics.isSetEnable(jb.getText(),size));
			if(logics.isOver(size)) { 
				System.exit(0);
			}
		};
		
		ActionListener pr = e -> {
			System.out.println(logics.toPrint());
		};
		
		for(int i=0; i<size; i++) {
			JButton bt = new JButton("0");
			buttons.put(bt, i);
			bt.addActionListener(ac);
			this.getContentPane().add(bt);
		}
		
		JButton print = new JButton("Print");
		print.addActionListener(pr);
		this.getContentPane().add(print);

		this.setVisible(true);

	}

}
