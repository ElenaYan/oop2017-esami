package a01a.e2;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;

public class LogicsImpl implements Logics {

	Map<Integer, String> map;
	
	public LogicsImpl() {
		this.map = new HashMap<>();
		map.put(0, "0");
		map.put(1, "0");
		map.put(2, "0");
		map.put(3, "0");
	}

	@Override
	public boolean isSetEnable(String actual, int n) {
		return (Integer.parseInt(actual)!=n);
	}

	@Override
	public boolean isOver(int size) {	
		return (int)map.values().stream().distinct().count()==1;
	}

	@Override
	public String setNumber(Integer button) {
		Integer ris = Integer.parseInt(map.get(button)) + 1;
		map.replace(button, ris.toString());
		return ris.toString();
	}

	@Override
	public String toPrint() {
		return "<<" + String.join("|", map.values()) + ">>";
	}

}
