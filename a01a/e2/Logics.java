package a01a.e2;

public interface Logics {
	
	boolean isSetEnable(String actual, int size);
	
	boolean isOver(int size);
	
	String setNumber(Integer button);
	
	String toPrint();
}
