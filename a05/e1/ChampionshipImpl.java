package a05.e1;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class ChampionshipImpl implements Championship {
	private List<String> registerTeams = new LinkedList<>();
	private Boolean canRegister = true;
	private Boolean isPlay = false;
	private Integer day = 0;
	private Set<Match> matchs = new HashSet<>();
	
	@Override
	public void registerTeam(String name) {
		if(this.canRegister) {
			this.registerTeams.add(name);
		}
	}

	@Override
	public void startChampionship() {
		this.canRegister=false;
	}

	@Override
	public void newDay() {
		if(this.day<=(this.registerTeams.size()-1)*2) {
			this.day++;
		}
		else {
			this.day=-1;
		}
	}

	@Override
	public Set<Match> pendingMatches() {
		if(this.day!=0 && !this.isPlay) {
			Optional<String> t1 = Optional.empty();
			for(String team : this.registerTeams) {
				if(t1.isEmpty()) {
					t1=Optional.of(team);
				} else {
					this.matchs.add(new MatchImpl(t1.get(), team));
					t1=Optional.empty();
				}
			}
		} 
		System.out.println(this.matchs);
		return this.matchs;
	}

	@Override
	public void matchPlay(Match match, int homeGoals, int awayGoals) {
		this.isPlay = true;
		this.matchs.remove(match);

	}

	@Override
	public Map<String, Integer> getClassification() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean championshipOver() {
		this.newDay();
		return (this.day==-1);
	}

}
