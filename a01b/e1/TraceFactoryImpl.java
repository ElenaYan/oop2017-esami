package a01b.e1;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

public class TraceFactoryImpl implements TraceFactory {
	
	@Override
	public <X> Trace<X> fromSuppliers(Supplier<Integer> sdeltaTime, Supplier<X> svalue, int size) {
		return new Trace<X>() {

			@Override
			public Optional<Event<X>> nextEvent() {
			
				return null;
			}

			@Override
			public Iterator<Event<X>> iterator() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void skipAfter(int time) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public Trace<X> combineWith(Trace<X> trace) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Trace<X> dropValues(X value) {
				// TODO Auto-generated method stub
				return null;
			}
			
		};
	}

	@Override
	public <X> Trace<X> constant(Supplier<Integer> sdeltaTime, X value, int size) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <X> Trace<X> discrete(Supplier<X> svalue, int size) {
		return new Trace<X>() {

			private List<Event<X>> list = new LinkedList<>();

			@Override
			public Optional<Event<X>> nextEvent() {
				return Optional.of(list.iterator().next());
			}

			@Override
			public Iterator<Event<X>> iterator() {
				return list.iterator();
			}

			@Override
			public void skipAfter(int time) {
				for (int i=0; i<time; i++) {
					list.iterator().next();
				}
			}

			@Override
			public Trace<X> combineWith(Trace<X> trace) {
				//Trace<X> t = new Trace<>();
				//list.stream().filter(l->l.)
				return this;
			}

			@Override
			public Trace<X> dropValues(X value) {
				// TODO Auto-generated method stub
				return this;
			}
			
		};
//		for(int i=0; i<size; i++) {
//			this.intList.add(i);
//			this.stringList.add(svalue.get().toString());
//		}
//		return String.join(";", pair);
	}

}
