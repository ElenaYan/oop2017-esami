package a01b.e1;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class TraceImpl<X> implements Trace<X> {
	List<Event<X>> list;

	public TraceImpl() {
		this.list = new LinkedList<>();
	}

	@Override
	public Optional<Event<X>> nextEvent() {
		return Optional.of(list.iterator().next());
	}

	@Override
	public Iterator<Event<X>> iterator() {
		return list.iterator();
	}

	@Override
	public void skipAfter(int time) {
		for (int i=0; i<time; i++) {
			list.iterator().next();
		}
	}

	@Override
	public Trace<X> combineWith(Trace<X> trace) {
		//Trace<X> t = new Trace<>();
		//list.stream().filter(l->l.)
		return null;
	}

	@Override
	public Trace<X> dropValues(X value) {
		// TODO Auto-generated method stub
		return null;
	}

}
