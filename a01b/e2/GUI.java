package a01b.e2;

import java.awt.*;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.List;
import java.util.stream.*;

import javax.swing.*;

public class GUI extends JFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7349574303073821702L;
	Logics logics = new LogicsImpl();
	List<JButton> list = new LinkedList<>();
	
	public GUI(int size){
		this.setSize(500, 100);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.getContentPane().setLayout(new FlowLayout());
		
		logics.generateRandomNumbers(size);
		
		ActionListener ac = (src)-> {
			JButton b =(JButton)src.getSource();
			Integer position = list.indexOf(b);
			System.out.println(logics.Counter());
			if(logics.hit(position)) {
				b.setEnabled(false);
				b.setText(logics.hitNumber(position));
			}
		};
		
		for(int i=0; i<size; i++) {
			JButton bt = new JButton("*");
			list.add(bt);
			bt.addActionListener(ac);
			this.getContentPane().add(bt);
		}
		
		this.setVisible(true);
	}
	
}
