package a01b.e2;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class LogicsImpl implements Logics {
	private int counter;
	private final List<Integer> numbers;

	public LogicsImpl() {
		this.counter = 1;
		this.numbers = new LinkedList<>();
	}

	@Override
	public void generateRandomNumbers(int size) {
		Random rand = new Random();		
		for(int i=0; i<size; i++) {
			numbers.add(rand.nextInt(99));
		}
	}
	
	@Override
	public Boolean hit(Integer position) {
		Integer min = numbers.stream().min((n1,n2) -> n1.compareTo(n2)).get();
		this.counter++;
		if(numbers.get(position) == min) {
			return true;
		}
		return false;
	}

	@Override
	public String Counter() {
		return "Tentativo n." + this.counter;
	}

	@Override
	public String hitNumber(Integer position) {
		String res = numbers.get(position).toString();
		numbers.set(position, 100);
		return res;
	}


}
