package a01b.e2;

public interface Logics {
	
	void generateRandomNumbers(int size); 
	
	Boolean hit(Integer position);
	
	String hitNumber(Integer position);
	
	String Counter();
}
