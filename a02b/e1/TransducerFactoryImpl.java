package a02b.e1;

import java.util.LinkedList;
import java.util.List;

public class TransducerFactoryImpl implements TransducerFactory {

	@Override
	public <X> Transducer<X, String> makeConcatenator(int inputSize) {
		return new Transducer<X, String>() {
			private List<X> list = new LinkedList<>();
			private int counter = 0;
			private boolean isOver = false;
			
 			@Override
			public void provideNextInput(X x) {
				list.add(x);
			}

			@Override
			public void inputIsOver() {
				if(!this.isOver) {
					this.isOver = true;
				}
				else {
					throw new IllegalStateException();
				}
			}

			@Override
			public boolean isNextOutputReady() {
				final Integer next = this.counter + 1;
				return next<list.size() || (this.isOver && next==list.size());
			}

			@Override
			public String getOutputElement() {
				if(isNextOutputReady()) {
					String out = new String();
					for(int i=0; i<inputSize; i++) {
						if(this.counter<list.size()) {
							out = out + list.get(counter++).toString();
						}
					}
					return out;
				}
				else {
					throw new IllegalStateException();
				}
			}

			@Override
			public boolean isOutputOver() {
				return (this.isOver && this.counter==this.list.size());
			}
			
		};
	}

	@Override
	public Transducer<Integer, Integer> makePairSummer() {
		return new Transducer<Integer, Integer>() {
			
			private List<Integer> list = new LinkedList<>();
			private boolean isOver = false;
			private int counter = 0;

			@Override
			public void provideNextInput(Integer x) {
				list.add(x);
			}

			@Override
			public void inputIsOver() {
				if(this.isOver == false) {
					this.isOver = true;
				} 
				else {
					throw new IllegalStateException();
				}
			}

			@Override
			public boolean isNextOutputReady() {
				int next = this.counter + 1;
				return next<list.size() || (this.isOver && next==list.size());
			}

			@Override
			public Integer getOutputElement() {
				int next1 = this.counter+1;
				if(isNextOutputReady()) {
					int next2 = this.counter+2;
					if(next2<list.size()) {
						return list.get(counter++) + list.get(counter++);
					}
					else {
						return list.get(counter++);
					}
				}
				else {
					throw new IllegalStateException();
				}
			}

			@Override
			public boolean isOutputOver() {
				return (this.isOver && this.counter==this.list.size());
			}
			
		}; 
	}

}
