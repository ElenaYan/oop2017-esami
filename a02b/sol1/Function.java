package a02b.sol1;

public interface Function<I,O> {
	O apply(I i);
}
