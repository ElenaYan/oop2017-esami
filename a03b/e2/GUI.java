package a03b.e2;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.*;

public class GUI extends JFrame {
    
     
    /**
	 * 
	 */
	private static final long serialVersionUID = 3940859503182067023L;
	private final Logics logics = new LogicsImpl();
	private Map<JButton, Pair<Integer, Integer>> buttons = new HashMap<>();

	public GUI(int size) {
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(500, 500);
        int cols = size; 
        JPanel panel = new JPanel(new GridLayout(cols,cols));
        this.getContentPane().add(BorderLayout.CENTER,panel);
        
        ActionListener al = (e)->{
            final JButton bt = (JButton)e.getSource();
            Pair<Integer, Integer> pos = buttons.get(bt);
            logics.hit(size, pos.getX(), pos.getY());
    		for(Entry<JButton, Pair<Integer, Integer>> entry : this.buttons.entrySet()) {
    			if(logics.setText(entry.getValue().getX(), entry.getValue().getY())) {
    				entry.getKey().setText("*");
    			}
    		}
        };
        
        for (int i=0;i<cols;i++){
	            for (int j=0;j<cols;j++){
	            final JButton jb = new JButton(" ");
	            jb.addActionListener(al);
	            panel.add(jb);
	            this.buttons.put(jb, new Pair<>(i,j));
            }
        } 
        this.setVisible(true);
    }
    
    
}
