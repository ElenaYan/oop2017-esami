package a03b.e2;

import java.util.*;

public class LogicsImpl implements Logics {
	private Map<Pair<Integer, Integer>, Boolean> map = new HashMap<>();
	private Integer counterI = 0;
	private Integer counterJ = 0;

	private void init(int size) {
		this.counterI=0;
		this.counterJ=size;
        for (int i=0;i<size;i++){
            for (int j=0;j<size;j++){
            	this.map.put(new Pair<>(i,j), false);
            }
        }
	}
	
	@Override
	public void hit(int size, int i, int j) {
		this.init(size);
		System.out.println(i + " " + j);
		 for(int i1=0; i1<size; i1++) {
			 for(int j1=0; j1<size; j1++) {
				 if(i1+j1==i+j) {
					 this.map.replace(new Pair<>(i1,j1), true);
				 }
			 }
		 }
		
		System.out.println(map);
	}


	@Override
	public boolean setText(int i, int j) {
		return this.map.get(new Pair<>(i,j));
	}

}
