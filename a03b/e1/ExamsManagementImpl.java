package a03b.e1;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;

public class ExamsManagementImpl implements ExamsManagement {
	private static final Integer VOTOMIGLIORE = 60;
	private static final Integer VOTOPEGGIORE = 40; 
	Map<Integer, String> students = new HashMap<>();
	Map<Integer, List<Pair<Integer, Integer>>> labEvaluation = new HashMap<>();
	Map<Integer, Pair<Integer, String>> projEvaluation = new HashMap<>();
	
	@Override
	public void createStudent(int studentId, String name) {
		this.students.put(studentId, name);
	}

	@Override
	public void registerLabEvaluation(int studentId, int evaluation, int exam) {
		if(this.labEvaluation.containsKey(studentId)) {
			List<Pair<Integer, Integer>> list = this.labEvaluation.get(studentId);
			for(Pair<Integer, Integer> pair : list) {
				if(pair.getY()==exam) {
					throw new IllegalStateException();
				}
			}
			list.add(new Pair<>(evaluation, exam));
		}
		else {
			List<Pair<Integer, Integer>> list = new LinkedList<>();
			list.add(new Pair<>(evaluation, exam));
			this.labEvaluation.put(studentId, list);
		}
	}

	@Override
	public void registerProjectEvaluation(int studentId, int evaluation, String project) {
		if(!this.projEvaluation.containsKey(studentId)) {
			this.projEvaluation.put(studentId, new Pair<>(evaluation, project));
		} 
	}

	@Override
	public Optional<Integer> finalEvaluation(int studentId) {
		if(this.labEvaluation.containsKey(studentId) && this.projEvaluation.containsKey(studentId)) {
			List<Pair<Integer, Integer>> list = this.labEvaluation.get(studentId);
			Integer voto1 = list.get(list.size()-1).getX();
			Integer voto2 = this.projEvaluation.get(studentId).getX();
			Integer somma = null;
			if(voto1 > voto2) {
				somma = (voto1*VOTOMIGLIORE)+(voto2*VOTOPEGGIORE);
			}
			else {
				somma = (voto1*VOTOPEGGIORE)+(voto2*VOTOMIGLIORE);
			}
			return ((somma%100)>=50) ? Optional.ofNullable((somma/100)+1) : Optional.ofNullable(somma/100) ;
		}
		else {
			return Optional.empty();
		}
	}

	@Override
	public Map<String, Integer> labExamStudentToEvaluation(int exam) {
		Map<String, Integer> map = new HashMap<>();
		for(Entry<Integer, List<Pair<Integer, Integer>>> entry : this.labEvaluation.entrySet()) {
			for(Pair<Integer, Integer> value : entry.getValue()) {
					if(value.getY()==exam) {
						String name = this.students.get(entry.getKey());
						map.put(name, value.getX());
					}
				}
			}
		return map;
	}

	@Override
	public Map<String, Integer> allLabExamStudentToFinalEvaluation() {
		Map<String, Integer> map = new HashMap<>();
		for(Entry<Integer, String> entry : students.entrySet()) {
			Optional<Integer> voto = this.finalEvaluation(entry.getKey());
			if(voto.isPresent()) {
				map.put(entry.getValue(), voto.get());
			}
		}
		return map;
	}

	@Override
	public Map<String, Integer> projectEvaluation(String project) {
		// TODO Auto-generated method stub
		return null;
	}

}
