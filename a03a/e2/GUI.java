package a03a.e2;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.nio.file.Files;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class GUI {
	
	private final JFrame gui = new JFrame();
	private List<JButton> list = new LinkedList<>();
 
	public GUI(String fileName) throws IOException {
		gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		final JPanel main = new JPanel();
		main.setLayout(new BoxLayout(main, BoxLayout.Y_AXIS));
		File file = new File(fileName);
		
		Iterator<String> lines = Files.lines(file.toPath()).iterator();
		for(long i=0; i< Files.lines(file.toPath()).count(); i++) {
			JButton bt = new JButton(lines.next());
			main.add(bt);
			bt.addActionListener(e -> ((JButton)e.getSource()).setEnabled(false));
			list.add(bt);
		}

		ActionListener write = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					PrintStream print = new PrintStream(file);
					
					list.forEach(b -> {
						if(b.isEnabled()) {
							print.print(b.getText()+"\n");
						}
						
					});
					
					System.exit(0);
					
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
			}
			
		};
		final JPanel outer = new JPanel(new BorderLayout());
		final JPanel center = new JPanel(new BorderLayout());
		outer.add(center,BorderLayout.SOUTH);
		outer.add(main,BorderLayout.NORTH);
		JButton close = new JButton("Close");
		close.addActionListener(write);
		center.add(close,BorderLayout.CENTER); // nell'esame il pulsante Close va messo però in basso, e possibilmente delle giuste dimensioni
		gui.getContentPane().add(outer);
		gui.setSize(600,300);
		gui.setVisible(true);
	}

}
