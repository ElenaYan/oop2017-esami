package a03a.e1;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class ExamsManagementImpl implements ExamsManagement {
	
	Set<Pair<Integer, String>> students = new HashSet<>(); 
	Set<Pair<String, Integer>> exams = new HashSet<>();
	Map<String, List<Integer>> registerStudent = new HashMap<>();
	Map<Integer, List<Pair<String,Integer>>> evaluation = new HashMap<>();
 	String currentExam = null;
 	Boolean examStart = false;
	
	@Override
	public void createStudent(int studentId, String name) {
		students.add(new Pair<>(studentId, name));
	}

	@Override
	public void createExam(String examName, int incrementalId) {
		exams.add(new Pair<>(examName, incrementalId));
	}

	@Override
	public void registerStudent(String examName, int studentId) {
		if(this.registerStudent.containsKey(examName)) {
			this.registerStudent.get(examName).add(studentId);
		}
		else {
			List<Integer> list = new LinkedList<>();
			list.add(studentId);
			this.registerStudent.put(examName, list);
		}

	}

	@Override
	public void examStarted(String examName) {
		this.examStart = true;
		this.currentExam = examName;

	}

	@Override
	public void registerEvaluation(int studentId, int evaluation) {
		if(this.evaluation.containsKey(studentId)) {
			this.evaluation.get(studentId).add(new Pair<> (this.currentExam, evaluation));
		}
		else {
			List<Pair<String, Integer>> list = new LinkedList<>();
			list.add(new Pair<>(this.currentExam, evaluation));
			this.evaluation.put(studentId, list);
		}

	}

	@Override
	public void examFinished() {
		this.examStart = false;

	}

	@Override
	public Set<Integer> examList(String examName) {
		if(this.registerStudent.containsKey(examName)) {
			return this.registerStudent.get(examName).stream().collect(Collectors.toSet());
		} else {
			Set<Integer> set = new HashSet<>();
			return set;
		}
	}

	@Override
	public Optional<Integer> lastEvaluation(int studentId) {
		if(this.evaluation.containsKey(studentId)) {
			List<Pair<String, Integer>> list = this.evaluation.get(studentId);	
			return Optional.ofNullable(list.get(list.size()-1).getY());
		} else {
			return Optional.empty();
		}
	}

	@Override
	public Map<String, Integer> examStudentToEvaluation(String examName) {
		Map<String, Integer> map = new HashMap<>();
		Set<Integer> set = this.examList(examName);
		System.out.println(set);
		String name = null;
		Integer mark = null;
		
		for(Integer id : set) {
			for(Pair<Integer, String> student : this.students) {
				if(student.getX().equals(id)) {
					name = student.getY();
				}
			}
			
			if(this.evaluation.containsKey(id)) {
				for(Pair<String, Integer> e : this.evaluation.get(id)) {
					if(e.getX().equals(examName)) {
						mark = e.getY();
						map.put(name, mark);
					}
				}
			}
		} 
		return map;
	}

	@Override
	public Map<Integer, Integer> examEvaluationToCount(String examName) {
		Map<Integer, Integer> map = new HashMap<>();
		for(Integer mark : this.examStudentToEvaluation(examName).values()) {
			if(map.containsKey(mark)) {
				map.replace(mark, map.get(mark)+1);
			} 
			else {
				map.put(mark, 1);
			} 
		}
		return map;
	}

}
